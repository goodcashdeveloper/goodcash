'use strict';

var auth = angular.module('goodCash');

auth.factory("goodCashAuth",['$http','CONF','$cookies',function($http, CONF, $cookies) {
  return {
    getToken : function() {
      var token = $cookies.get('token');
      if (token == null) {
        return false;
      } else {
        return token;
      }
    },
    setData : function(data) {
      if (data != null) {
        $cookies.put('token',data.token);
        $cookies.put('email',data.email);
        $cookies.put('username',data.username);
        $cookies.put('user_id',data.user_id);
        return true;
      }else {
        return false;
      }
    },
    isAuth : function() {
      var token = $cookies.get('token');
      if (token == null || token == 'null') {
        return false;
      }else {
        // return $.ajax({
        //         type: 'GET',
        //         url: CONF.getUser+$cookies.get('user_id'),
        //         contentType: 'application/json;charset=utf-8',
        //         dataType: 'json',
        //         async: false,
        //         headers: {
        //           'Api-Key': CONF.api,
        //           'User-Token' : token,
        //         },
        // })
        // .done(function(data) {
        //   if (!data.error) {
        //     return true;
        //   }else {
        //     return false;
        //   }
        // })
        // .fail(function() {
        //   return false;
        // })
        return true;
      }
    },
    removeData : function() {
      $cookies.put('token',null);
      $cookies.put('email',null);
      $cookies.put('username',null);
      $cookies.put('user_id',null);
      return true;
    },
    getDetailUser : function() {
      var token = $cookies.get('token');
      if (token == null) {
        return false;
      }else {
        return $.ajax({
                type: 'GET',
                url: CONF.getUser+$cookies.get('user_id'),
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                headers: {
                  'Api-Key': CONF.api,
                  'User-Token' : token,
                },
        })
      }
    }
  }
}]);
