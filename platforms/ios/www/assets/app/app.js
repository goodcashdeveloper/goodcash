'use strict';

var app = angular.module('goodCash',['ui.router','ngStorage','ngCookies','dc.endlessScroll']);

app.run(function ($rootScope,$timeout) {
  $rootScope.$on('$viewContentLoaded', ()=> {
    $timeout(() => {
      componentHandler.upgradeAllRegistered();
    })
  })
});

app.config(['$stateProvider', '$locationProvider','$urlMatcherFactoryProvider','$urlRouterProvider','$httpProvider', function($stateProvider, $locationProvider, $urlMatcherFactoryProvider, $urlRouterProvider, $httpProvider){

  $urlRouterProvider.otherwise('/app/splash');
  $stateProvider
  .state("app", {
    url: "/app",
    abstract: true,
    template: '<div ui-view></div>'
  })
  .state("app.splash", {
    url: "/splash",
    templateUrl: "views/splash.html?v=1.0.0",
    controller: "goodCashSplash"
  })
  .state("app.login", {
    url: "/login",
    templateUrl: "views/login.html?v=1.1.1",
    controller: "goodCashLogin"
  })
  .state("app.wallet", {
    url: "/wallet",
    params: {obj:null},
    templateUrl: "views/wallet.html?v=1.1.4",
    controller: "goodCashWallet"
  })
  .state("app.history", {
    url: "/history",
    params: {obj:null},
    templateUrl: "views/history.html?v=1.1.4",
    controller: "goodCashHistory"
  })
  .state("app.logout", {
    url: "/logout",
    templateUrl: "views/logout.html?v=1.1.0",
    controller: "goodCashLogout"
  })
}]);

app.controller("goodCashSplash",['$scope', '$location', '$sessionStorage', '$localStorage', '$state', 'CONF','$http','goodCashAuth', function($scope, $location, $sessionStorage, $localStorage,$state,CONF,$http,goodCashAuth){
  setTimeout(function(){$state.go('app.logout')}, 3000);
}]);

app.controller("goodCashLogin",['$scope', '$location', '$sessionStorage', '$localStorage', '$timeout', '$state', 'CONF','$http','goodCashAuth', function($scope, $location, $sessionStorage, $localStorage, $timeout,$state,CONF,$http,goodCashAuth){
  $scope.loader = false;
  if (goodCashAuth.isAuth()) {
    var isauthenticated1 = true;
    $state.go('app.wallet',{obj: isauthenticated1});
  }else {
    var error_dialog = document.querySelector('#error_dialog');
    var success_dialog = document.querySelector('#success_dialog');
    var success_dialog2 = document.querySelector('#success_dialog2');
    var sign_dialog = document.querySelector('#signup_dialog');
    var forgot_dialog = document.querySelector('#forgot_dialog');
    if (! sign_dialog.showModal) {
      dialogPolyfill.registerDialog(sign_dialog);
    }
    if (! forgot_dialog.showModal) {
      dialogPolyfill.registerDialog(forgot_dialog);
    }
    if (! error_dialog.showModal) {
      dialogPolyfill.registerDialog(error_dialog);
    }
    if (! success_dialog.showModal) {
      dialogPolyfill.registerDialog(success_dialog);
    }
    if (! success_dialog2.showModal) {
      dialogPolyfill.registerDialog(success_dialog2);
    }

    $scope.login_title = 'Welcome to Good Cash';
    $scope.buttonLogin = false;

    $scope.openSignUp = function() {
      sign_dialog.showModal();
      sign_dialog.addEventListener('click', function (event) {
          var rect = sign_dialog.getBoundingClientRect();
          var isInDialog=(rect.top <= event.clientY && event.clientY <= rect.top + rect.height
            && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
          if (!isInDialog && document.getElementById("signup_dialog").open) {
              sign_dialog.close();
          }
      });
    }

    $scope.openForgetPassword = function() {
      forgot_dialog.showModal();
      forgot_dialog.addEventListener('click', function (event) {
          var rect = forgot_dialog.getBoundingClientRect();
          var isInDialog=(rect.top <= event.clientY && event.clientY <= rect.top + rect.height
            && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
          if (!isInDialog && document.getElementById("forgot_dialog").open) {
              forgot_dialog.close();
          }
      });
    }

    $scope.submitForm = function() {
      $scope.loader = true;
      $.ajax({
              type: 'POST',
              url: CONF.login,
              contentType: 'application/json;charset=utf-8',
              dataType: 'json',
              data: JSON.stringify({
                'email' : $scope.email,
                'password' : $scope.password,
              }),
              headers: {
                'Api-Key': CONF.api,
              },
      })
      .done(function(data) {
        $scope.loader = false;
        if (!data.error) {
          if (goodCashAuth.setData(data.data)) {
            var isauthenticated = true;
            $state.go('app.wallet',{obj: isauthenticated});
          }
        }else {
          $scope.$apply(function () {
           $scope.error_message = data.message;
          });
          error_dialog.showModal();
        }
      })
      .fail(function(data) {
        $scope.loader = false;
        $scope.$apply(function () {
          if (data.status == 0) {
            $scope.error_message = 'please check your internet connection.';
          }else {
            $scope.error_message = JSON.parse(data.responseText)['message'];
          }
        });
        error_dialog.showModal();
      })
    }

    $scope.registerUser = function() {
      if ($scope.regist_password !== $scope.regist_password_conf) {
        $scope.success_message = "password confirmation doesn't match password";
        success_dialog.showModal();
      }else {
        sign_dialog.close();
        $scope.loader = true;
        var dataRegister = {
          email : $scope.regist_email,
          username : $scope.regist_username,
          password : $scope.regist_password,
        }
        $.ajax({
                type: 'POST',
                url: CONF.createUser,
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                data: JSON.stringify(dataRegister),
                headers: {
                  'Api-Key': CONF.api,
                },
        })
        .done(function(data) {
          $scope.loader = false;
          if (!data.error) {
            $scope.$apply(function () {
             $scope.email_sent = $scope.regist_email;
            });
            success_dialog2.showModal();
          }else {
            $scope.$apply(function () {
             $scope.success_message = data.message;
            });
            success_dialog.showModal();
          }
        })
        .fail(function(data) {
          $scope.loader = false;
          var message = JSON.parse(data.responseText);
          $scope.$apply(function () {
           $scope.success_message = message['message'];
          });
          success_dialog.showModal();
        })
      }
    }

    $scope.sendEmailForgot = function() {
      forgot_dialog.close();
      $scope.loader = true;
      var dataForgot = {
        email : $scope.forgot_email
      }
      $.ajax({
              type: 'PUT',
              url: CONF.forgetPassword,
              data: dataForgot,
              headers: {
                'Api-Key': CONF.api,
              },
      })
      .done(function(data) {
        $scope.loader = false;
        if (!data.error) {
          $scope.$apply(function () {
           $scope.success_message = data.message;
          });
          success_dialog.showModal();
        }else {
          $scope.$apply(function () {
           $scope.success_message = data.message;
          });
          success_dialog.showModal();
        }
      })
      .fail(function(data) {
        $scope.loader = false;
        var message = JSON.parse(data.responseText);
        $scope.$apply(function () {
         $scope.success_message = message['message'];
        });
        success_dialog.showModal();
      })
    }

    $scope.closeModal = function() {
      error_dialog.close();
    }

    $scope.closeModal_3 = function() {
      success_dialog.close();
    }

    $scope.closeModal_4 = function() {
      success_dialog2.close();
    }

    $scope.closeModal_forgot = function() {
      forgot_dialog.close();
    }

    $scope.closeModal_signup = function() {
      sign_dialog.close();
    }

  }

}]);

app.controller("goodCashLogout",['$scope', '$location', '$sessionStorage', '$localStorage', '$timeout', '$state', 'CONF','$http','goodCashAuth','$cookies', function($scope, $location, $sessionStorage, $localStorage, $timeout,$state,CONF,$http,goodCashAuth,$cookies){
  $scope.loader = true;
   var token = $cookies.get('token');
   if (token == null || token == '' ) {
     $state.go('app.login');
   }else {
     $.ajax({
             type: 'DELETE',
             url: CONF.logout,
             contentType: 'application/json;charset=utf-8',
             dataType: 'json',
             headers: {
               'Api-Key': CONF.api,
               'User-Token' : token,
             },
     })
     .done(function(data) {
       goodCashAuth.removeData();
       if (!data.error) {
         $state.go('app.login');
       }else {
         $state.go('app.login');
       }
     })
     .fail(function(data) {
       goodCashAuth.removeData();
       $state.go('app.login');
     })
   }
}]);

app.controller("goodCashWallet",['$scope', '$location', '$sessionStorage', '$localStorage', '$timeout', '$state', 'CONF','$http','goodCashAuth','$cookies', function($scope, $location, $sessionStorage, $localStorage, $timeout,$state,CONF,$http,goodCashAuth,$cookies){
  if($state.params.obj != null){
    if ($state.params.obj) {
      $scope.isauthenticatedWallet = true;
    }else {
      $state.go('app.login');
    }
  }else {
    if(goodCashAuth.isAuth()){
      $scope.isauthenticatedWallet = true;
    }else {
      $state.go('app.login');
    }
  }
  if ($scope.isauthenticatedWallet) {
    var pocket_1 = document.querySelector('#pocket_1');
    if (! pocket_1.showModal) {
      dialogPolyfill.registerDialog(pocket_1);
    }
    var pocket_2 = document.querySelector('#pocket_2');
    if (! pocket_2.showModal) {
      dialogPolyfill.registerDialog(pocket_2);
    }
    var pocket_3 = document.querySelector('#pocket_3');
    if (! pocket_3.showModal) {
      dialogPolyfill.registerDialog(pocket_3);
    }
    var pocket_4 = document.querySelector('#pocket_4');
    if (! pocket_4.showModal) {
      dialogPolyfill.registerDialog(pocket_4);
    }
    var success_transaction = document.querySelector('#success_transaction');
    if (! success_transaction.showModal) {
      dialogPolyfill.registerDialog(success_transaction);
    }
    var error_dialog = document.querySelector('#error_dialog');
    if (! error_dialog.showModal) {
      dialogPolyfill.registerDialog(error_dialog);
    }

    $scope.username = $cookies.get('username');
    $scope.loader = true;

    $scope.getBalances = function() {
      $.ajax({
              type: 'GET',
              url: CONF.getbalances+$cookies.get('user_id')+'/balances',
              headers: {
                'User-Token': $cookies.get('token'),
              },
      })
      .done(function(data) {
        $scope.$apply(function () {
         $scope.loader = false;
        });
        if (!data.error) {
          $scope.$apply(function () {
            $scope.total_balances = 0;
            var data_balances = data.data;
            for (var i in data_balances) {
              $scope.total_balances += parseInt(data_balances[i].balance);
              if (data_balances[i].pocket_id == '1') {
                $scope.balances_1 = parseInt(data_balances[i].balance);
              }else if (data_balances[i].pocket_id == '2') {
                $scope.balances_2 = parseInt(data_balances[i].balance);
              }else if (data_balances[i].pocket_id == '3') {
                $scope.balances_3 = parseInt(data_balances[i].balance);
              }else if (data_balances[i].pocket_id == '4') {
                $scope.balances_4 = parseInt(data_balances[i].balance);
              }
            }
          });
        }else {
          $state.go('app.logout');
        }
      })
      .fail(function(data) {
        $scope.$apply(function () {
         $scope.loader = false;
        });
        $state.go('app.logout');
      })
    }

    $scope.getBalances();

    $scope.openPocket = function(pocket_id) {
      if (pocket_id == 1) {
         $scope.clickCashout(1);
         $scope.chipActive_1('foodchip_1');
         $scope.buttonSubmit_1 = false;
         $scope.balancis_1 = false;
         pocket_1.showModal();
         pocket_1.addEventListener('click', function (event) {
             var rect = pocket_1.getBoundingClientRect();
             var isInDialog=(rect.top <= event.clientY && event.clientY <= rect.top + rect.height
               && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
             if (!isInDialog && document.getElementById("pocket_1").open) {
                 pocket_1.close();
             }
         });
      }else if (pocket_id == 2) {
        $scope.clickCashout(2);
        $scope.chipActive_2('transportchip_2');
        $scope.buttonSubmit_2 = false;
        $scope.balancis_2 = false;
        pocket_2.showModal();
        pocket_2.addEventListener('click', function (event) {
            var rect = pocket_2.getBoundingClientRect();
            var isInDialog=(rect.top <= event.clientY && event.clientY <= rect.top + rect.height
              && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog && document.getElementById("pocket_2").open) {
                pocket_2.close();
            }
        });
      }else if (pocket_id == 3) {
        $scope.clickCashout(3);
        $scope.chipActive_3('entertainmentchip_3');
        $scope.buttonSubmit_3 = false;
        $scope.balancis_3 = false;
        pocket_3.showModal();
        pocket_3.addEventListener('click', function (event) {
            var rect = pocket_3.getBoundingClientRect();
            var isInDialog=(rect.top <= event.clientY && event.clientY <= rect.top + rect.height
              && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog && document.getElementById("pocket_3").open) {
                pocket_3.close();
            }
        });
      }else if (pocket_id == 4) {
        $scope.clickCashout(4);
        $scope.chipActive_4('othercip_4');
        $scope.buttonSubmit_4 = false;
        $scope.balancis_4 = false;
        pocket_4.showModal();
        pocket_4.addEventListener('click', function (event) {
            var rect = pocket_4.getBoundingClientRect();
            var isInDialog=(rect.top <= event.clientY && event.clientY <= rect.top + rect.height
              && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
            if (!isInDialog && document.getElementById("pocket_4").open) {
                pocket_4.close();
            }
        });
      }
    }

    $scope.clickCashout = function(pocket_id) {
      if (pocket_id == 1) {
        $scope.transaction_type_id_1 = 2;
        $scope.imageCashout_1 = '20 - CashOut Green.png';
        $scope.imageCashin_1 = '17 - CashIn Deactive.png';
        $scope.cashOut_1 = true;
        $scope.activeSubmit(1);
        $scope.activeSubmitViaNotes(1);
      }else if (pocket_id == 2) {
        $scope.transaction_type_id_2 = 2;
        $scope.imageCashout_2 = '22 - CashOut Red.png';
        $scope.imageCashin_2 = '17 - CashIn Deactive.png';
        $scope.cashOut_2 = true;
        $scope.activeSubmit(2);
        $scope.activeSubmitViaNotes(2);
      }else if (pocket_id == 3) {
        $scope.transaction_type_id_3 = 2;
        $scope.imageCashout_3 = '18 - Cashout Blue.png';
        $scope.imageCashin_3 = '17 - CashIn Deactive.png';
        $scope.cashOut_3 = true;
        $scope.activeSubmit(3);
        $scope.activeSubmitViaNotes(3);
      }else if (pocket_id == 4) {
        $scope.transaction_type_id_4 = 2;
        $scope.imageCashout_4 = '24 - CashOut Yellow.png';
        $scope.imageCashin_4 = '17 - CashIn Deactive.png';
        $scope.cashOut_4 = true;
        $scope.activeSubmit(4);
        $scope.activeSubmitViaNotes(4);
      }
    }

    $scope.clickCashin = function(pocket_id) {
      if (pocket_id == 1) {
        $scope.transaction_type_id_1 = 1;
        $scope.imageCashout_1 = '16 - CashOut Deactive.png';
        $scope.imageCashin_1 = '21 - CashIn Green.png';
        $scope.cashOut_1 = false;
        $scope.activeSubmit(1);
        $scope.activeSubmitViaNotes(1);
      }else if (pocket_id == 2) {
        $scope.transaction_type_id_2 = 1;
        $scope.imageCashout_2 = '16 - CashOut Deactive.png';
        $scope.imageCashin_2 = '23 - CashIn Red.png';
        $scope.cashOut_2 = false;
        $scope.activeSubmit(2);
        $scope.activeSubmitViaNotes(2);
      }else if (pocket_id == 3) {
        $scope.transaction_type_id_3 = 1;
        $scope.imageCashout_3 = '16 - CashOut Deactive.png';
        $scope.imageCashin_3 = '19 - CashIn Blue.png';
        $scope.cashOut_3 = false;
        $scope.activeSubmit(3);
        $scope.activeSubmitViaNotes(3);
      }else if (pocket_id == 4) {
        $scope.transaction_type_id_4 = 1;
        $scope.imageCashout_4 = '16 - CashOut Deactive.png';
        $scope.imageCashin_4 = '25 - CashIn Yellow.png';
        $scope.cashOut_4 = false;
        $scope.activeSubmit(4);
        $scope.activeSubmitViaNotes(4);
      }
    }

    $scope.chipActive_1 = function(id_chip) {
      $('.mdl-chip').removeClass('mdl-chip-active-1');
      $('#'+id_chip).addClass('mdl-chip-active-1');
      if (id_chip == 'foodchip_1') {
        $scope.purpose_id_1 = 1;
      }else if (id_chip == 'transportchip_1') {
        $scope.purpose_id_1 = 2;
      }else if (id_chip == 'entertainmentchip_1') {
        $scope.purpose_id_1 = 3;
      }else {
        $scope.purpose_id_1 = 4;
      }
    }

    $scope.chipActive_2 = function(id_chip) {
      $('.mdl-chip').removeClass('mdl-chip-active-2');
      $('#'+id_chip).addClass('mdl-chip-active-2');
      if (id_chip == 'foodchip_2') {
        $scope.purpose_id_2 = 1;
      }else if (id_chip == 'transportchip_2') {
        $scope.purpose_id_2 = 2;
      }else if (id_chip == 'entertainmentchip_2') {
        $scope.purpose_id_2 = 3;
      }else {
        $scope.purpose_id_2 = 4;
      }
    }

    $scope.chipActive_3 = function(id_chip) {
      $('.mdl-chip').removeClass('mdl-chip-active-3');
      $('#'+id_chip).addClass('mdl-chip-active-3');
      if (id_chip == 'foodchip_3') {
        $scope.purpose_id_3 = 1;
      }else if (id_chip == 'transportchip_3') {
        $scope.purpose_id_3 = 2;
      }else if (id_chip == 'entertainmentchip_3') {
        $scope.purpose_id_3 = 3;
      }else {
        $scope.purpose_id_3 = 4;
      }
    }

    $scope.chipActive_4 = function(id_chip) {
      $('.mdl-chip').removeClass('mdl-chip-active-4');
      $('#'+id_chip).addClass('mdl-chip-active-4');
      if (id_chip == 'foodchip_4') {
        $scope.purpose_id_4 = 1;
      }else if (id_chip == 'transportchip_4') {
        $scope.purpose_id_4 = 2;
      }else if (id_chip == 'entertainmentchip_4') {
        $scope.purpose_id_4 = 3;
      }else {
        $scope.purpose_id_4 = 4;
      }
    }

    $scope.activeSubmit = function(pocket_id) {
      if (pocket_id == 1) {
        if ($scope.cashbalance_1 != null && $scope.cashbalance_1 != '') {
          if ($scope.transaction_type_id_1 == 2) {
            if ($scope.cashbalance_1 > $scope.total_balances) {
              $scope.nextActiveButtonViaNotes1 = false;
              $scope.balancis_1 = false;
              $scope.error_message = 'Balance is not enough';
              error_dialog.showModal();
            }else {
              if ($scope.cashbalance_1 > $scope.balances_1) {
                $scope.nextActiveButtonViaNotes1 = false;
                $scope.balancis_1 = true;
              }else {
                $scope.nextActiveButtonViaNotes1 = true;
                $scope.balancis_1 = false;
              }
            }
          }else if ($scope.transaction_type_id_1 == 1) {
            $scope.nextActiveButtonViaNotes1 = true;
            $scope.balancis_1 = false;
          }
        }else {
          $scope.balancis_1 = false;
          $scope.nextActiveButtonViaNotes1 = false;
        }
        $scope.activeSubmitViaNotes(1);
      }else if (pocket_id == 2) {
        if ($scope.cashbalance_2 != null && $scope.cashbalance_2 != '') {
          if ($scope.transaction_type_id_2 == 2) {
            if ($scope.cashbalance_2 > $scope.total_balances) {
              $scope.nextActiveButtonViaNotes2 = false;
              $scope.balancis_2 = false;
              $scope.error_message = 'Balance is not enough';
              error_dialog.showModal();
            }else {
              if ($scope.cashbalance_2 > $scope.balances_2) {
                $scope.nextActiveButtonViaNotes2 = false;
                $scope.balancis_2 = true;
              }else {
                $scope.nextActiveButtonViaNotes2 = true;
                $scope.balancis_2 = false;
              }
            }
          }else if ($scope.transaction_type_id_2 == 1) {
            $scope.nextActiveButtonViaNotes2 = true;
            $scope.balancis_2 = false;
          }
        }else {
          $scope.balancis_2 = false;
          $scope.nextActiveButtonViaNotes2 = false;
        }
        $scope.activeSubmitViaNotes(2);
      }else if (pocket_id == 3) {
        if ($scope.cashbalance_3 != null && $scope.cashbalance_3 != '') {
          if ($scope.transaction_type_id_3 == 2) {
            if ($scope.cashbalance_3 > $scope.total_balances) {
              $scope.nextActiveButtonViaNotes3 = false;
              $scope.balancis_3 = false;
              $scope.error_message = 'Balance is not enough';
              error_dialog.showModal();
            }else {
              if ($scope.cashbalance_3 > $scope.balances_3) {
                $scope.nextActiveButtonViaNotes3 = false;
                $scope.balancis_3 = true;
              }else {
                $scope.nextActiveButtonViaNotes3 = true;
                $scope.balancis_3 = false;
              }
            }
          }else if ($scope.transaction_type_id_3 == 1) {
            $scope.nextActiveButtonViaNotes3 = true;
            $scope.balancis_3 = false;
          }
        }else {
          $scope.balancis_3 = false;
          $scope.nextActiveButtonViaNotes3 = false;
        }
        $scope.activeSubmitViaNotes(3);
      }else if (pocket_id == 4) {
        if ($scope.cashbalance_4 != null && $scope.cashbalance_4 != '') {
          if ($scope.transaction_type_id_4 == 2) {
            if ($scope.cashbalance_4 > $scope.total_balances) {
              $scope.nextActiveButtonViaNotes4 = false;
              $scope.balancis_4 = false;
              $scope.error_message = 'Balance is not enough';
              error_dialog.showModal();
            }else {
              if ($scope.cashbalance_4 > $scope.balances_4) {
                $scope.nextActiveButtonViaNotes4 = false;
                $scope.balancis_4 = true;
              }else {
                $scope.nextActiveButtonViaNotes4 = true;
                $scope.balancis_4 = false;
              }
            }
          }else if ($scope.transaction_type_id_4 == 1) {
            $scope.nextActiveButtonViaNotes4 = true;
            $scope.balancis_4 = false;
          }
        }else {
          $scope.balancis_4 = false;
          $scope.nextActiveButtonViaNotes4 = false;
        }
        $scope.activeSubmitViaNotes(4);
      }
    }

    $scope.aditionalBalances_1 = function(id) {
      var val = ($('#'+id).val() == null || $('#'+id).val() == '') ? 0 : $('#'+id).val();
      var min = 0;
      if (id == 'balancis_1_red') {
        var blue = ($scope.balancis_1_blue == null || $scope.balancis_1_blue == '') ? 0 : $scope.balancis_1_blue;
        var yellow = ($scope.balancis_1_yellow == null || $scope.balancis_1_yellow == '') ? 0 : $scope.balancis_1_yellow;
        var max = $scope.cashbalance_1 - ($scope.balances_1 + blue + yellow);
        if (max <= 0) {
          $('#'+id).val(0);
          $scope.balancis_1_red = 0;
          $('#background-balancis-red-1').css('background-color','#dedede');
        }else {
          if (val > max) {
            if (max > $scope.balances_2) {
              $('#'+id).val($scope.balances_2);
              $scope.balancis_1_red = $scope.balances_2;
            }else {
              $('#'+id).val(max);
              $scope.balancis_1_red = max;
            }
            $('#background-balancis-red-1').css('background-color','#ff7388');
          }else if (val < min) {
            $('#'+id).val(0);
            $scope.balancis_1_red = 0;
            $('#background-balancis-red-1').css('background-color','#dedede');
          }else {
            if (val == null || val == '' || val == 0) {
              $('#'+id).val(0);
              $scope.balancis_1_red = 0;
              $('#background-balancis-red-1').css('background-color','#dedede');
            }else {
              $('#background-balancis-red-1').css('background-color','#ff7388');
            }
          }
        }
        if ($scope.cashbalance_1 == ($scope.balances_1 + blue + yellow + $scope.balancis_1_red)) {
          $scope.nextActiveButtonViaNotes1 = true;
        }else {
          $scope.nextActiveButtonViaNotes1 = false;
        }
        $scope.activeSubmitViaNotes(1);
      }else if (id == 'balancis_1_blue') {
        var red = ($scope.balancis_1_red == null || $scope.balancis_1_red == '') ? 0 : $scope.balancis_1_red;
        var yellow = ($scope.balancis_1_yellow == null || $scope.balancis_1_yellow == '') ? 0 : $scope.balancis_1_yellow;
        var max = $scope.cashbalance_1 - ($scope.balances_1 + red + yellow);
        if (max <= 0) {
          $('#'+id).val(0);
          $scope.balancis_1_blue = 0;
          $('#background-balancis-blue-1').css('background-color','#dedede');
        }else {
          if (val > max) {
            if (max > $scope.balances_3) {
              $('#'+id).val($scope.balances_3);
              $scope.balancis_1_blue = $scope.balances_3;
            }else {
              $('#'+id).val(max);
              $scope.balancis_1_blue = max;
            }
            $('#background-balancis-blue-1').css('background-color','#00bcd4');
          }else if (val < min) {
            $('#'+id).val(0);
            $scope.balancis_1_blue = 0;
            $('#background-balancis-blue-1').css('background-color','#dedede');
          }else {
            if (val == null || val == '' || val == 0) {
              $('#'+id).val(0);
              $scope.balancis_1_blue = 0;
              $('#background-balancis-blue-1').css('background-color','#dedede');
            }else {
              $('#background-balancis-blue-1').css('background-color','#00bcd4');
            }
          }
        }
        if ($scope.cashbalance_1 == ($scope.balances_1 + red + yellow + $scope.balancis_1_blue)) {
          $scope.nextActiveButtonViaNotes1 = true;
        }else {
          $scope.nextActiveButtonViaNotes1 = false;
        }
        $scope.activeSubmitViaNotes(1);
      }else if (id == 'balancis_1_yellow') {
        var red = ($scope.balancis_1_red == null || $scope.balancis_1_red == '') ? 0 : $scope.balancis_1_red;
        var blue = ($scope.balancis_1_blue == null || $scope.balancis_1_blue == '') ? 0 : $scope.balancis_1_blue;
        var max = $scope.cashbalance_1 - ($scope.balances_1 + red + blue);
        if (max <= 0) {
          $('#'+id).val(0);
          $scope.balancis_1_yellow = 0;
          $('#background-balancis-yellow-1').css('background-color','#dedede');
        }else {
          if (val > max) {
            if (max > $scope.balances_4) {
              $('#'+id).val($scope.balances_4);
              $scope.balancis_1_yellow = $scope.balances_4;
            }else {
              $('#'+id).val(max);
              $scope.balancis_1_yellow = max;
            }
            $('#background-balancis-yellow-1').css('background-color','#f9db0a');
          }else if (val < min) {
            $('#'+id).val(0);
            $scope.balancis_1_yellow = 0;
            $('#background-balancis-yellow-1').css('background-color','#dedede');
          }else {
            if (val == null || val == '' || val == 0) {
              $('#'+id).val(0);
              $scope.balancis_1_yellow = 0;
              $('#background-balancis-yellow-1').css('background-color','#dedede');
            }else {
              $('#background-balancis-yellow-1').css('background-color','#f9db0a');
            }
          }
        }
        if ($scope.cashbalance_1 == ($scope.balances_1 + blue + red + $scope.balancis_1_yellow)) {
          $scope.nextActiveButtonViaNotes1 = true;
        }else {
          $scope.nextActiveButtonViaNotes1 = false;
        }
        $scope.activeSubmitViaNotes(1);
      }
    }

    $scope.aditionalBalances_2 = function(id) {
      var val = ($('#'+id).val() == null || $('#'+id).val() == '') ? 0 : $('#'+id).val();
      var min = 0;
      if (id == 'balancis_2_green') {
        var blue = ($scope.balancis_2_blue == null || $scope.balancis_2_blue == '') ? 0 : $scope.balancis_2_blue;
        var yellow = ($scope.balancis_2_yellow == null || $scope.balancis_2_yellow == '') ? 0 : $scope.balancis_2_yellow;
        var max = $scope.cashbalance_2 - ($scope.balances_2 + blue + yellow);
        if (max <= 0) {
          $('#'+id).val(0);
          $scope.balancis_2_green = 0;
          $('#background-balancis-green-2').css('background-color','#dedede');
        }else {
          if (val > max) {
            if (max > $scope.balances_1) {
              $('#'+id).val($scope.balances_1);
              $scope.balancis_2_green = $scope.balances_1;
            }else {
              $('#'+id).val(max);
              $scope.balancis_2_green = max;
            }
            $('#background-balancis-green-2').css('background-color','#71b58b');
          }else if (val < min) {
            $('#'+id).val(0);
            $scope.balancis_2_green = 0;
            $('#background-balancis-green-2').css('background-color','#dedede');
          }else {
            if (val == null || val == '' || val == 0) {
              $('#'+id).val(0);
              $scope.balancis_2_green = 0;
              $('#background-balancis-green-2').css('background-color','#dedede');
            }else {
              $('#background-balancis-green-2').css('background-color','#71b58b');
            }
          }
        }
        if ($scope.cashbalance_2 == ($scope.balances_2 + blue + yellow + $scope.balancis_2_green)) {
          $scope.nextActiveButtonViaNotes2 = true;
        }else {
          $scope.nextActiveButtonViaNotes2 = false;
        }
        $scope.activeSubmitViaNotes(2);
      }else if (id == 'balancis_2_blue') {
        var green = ($scope.balancis_2_green == null || $scope.balancis_2_green == '') ? 0 : $scope.balancis_2_green;
        var yellow = ($scope.balancis_2_yellow == null || $scope.balancis_2_yellow == '') ? 0 : $scope.balancis_2_yellow;
        var max = $scope.cashbalance_2 - ($scope.balances_2 + green + yellow);
        if (max <= 0) {
          $('#'+id).val(0);
          $scope.balancis_2_blue = 0;
          $('#background-balancis-blue-2').css('background-color','#dedede');
        }else {
          if (val > max) {
            if (max > $scope.balances_3) {
              $('#'+id).val($scope.balances_3);
              $scope.balancis_2_blue = $scope.balances_3;
            }else {
              $('#'+id).val(max);
              $scope.balancis_2_blue = max;
            }
            $('#background-balancis-blue-2').css('background-color','#00bcd4');
          }else if (val < min) {
            $('#'+id).val(0);
            $scope.balancis_2_blue = 0;
            $('#background-balancis-blue-2').css('background-color','#dedede');
          }else {
            if (val == null || val == '' || val == 0) {
              $('#'+id).val(0);
              $scope.balancis_2_blue = 0;
              $('#background-balancis-blue-2').css('background-color','#dedede');
            }else {
              $('#background-balancis-blue-2').css('background-color','#00bcd4');
            }
          }
        }
        if ($scope.cashbalance_2 == ($scope.balances_2 + green + yellow + $scope.balancis_2_blue)) {
          $scope.nextActiveButtonViaNotes2 = true;
        }else {
          $scope.nextActiveButtonViaNotes2 = false;
        }
        $scope.activeSubmitViaNotes(2);
      }else if (id == 'balancis_2_yellow') {
        var green = ($scope.balancis_2_green == null || $scope.balancis_2_green == '') ? 0 : $scope.balancis_2_green;
        var blue = ($scope.balancis_2_blue == null || $scope.balancis_2_blue == '') ? 0 : $scope.balancis_2_blue;
        var max = $scope.cashbalance_2 - ($scope.balances_2 + green + blue);
        if (max <= 0) {
          $('#'+id).val(0);
          $scope.balancis_2_yellow = 0;
          $('#background-balancis-yellow-2').css('background-color','#dedede');
        }else {
          if (val > max) {
            if (max > $scope.balances_4) {
              $('#'+id).val($scope.balances_4);
              $scope.balancis_2_yellow = $scope.balances_4;
            }else {
              $('#'+id).val(max);
              $scope.balancis_2_yellow = max;
            }
            $('#background-balancis-yellow-2').css('background-color','#f9db0a');
          }else if (val < min) {
            $('#'+id).val(0);
            $scope.balancis_2_yellow = 0;
            $('#background-balancis-yellow-2').css('background-color','#dedede');
          }else {
            if (val == null || val == '' || val == 0) {
              $('#'+id).val(0);
              $scope.balancis_2_yellow = 0;
              $('#background-balancis-yellow-2').css('background-color','#dedede');
            }else {
              $('#background-balancis-yellow-2').css('background-color','#f9db0a');
            }
          }
        }
        if ($scope.cashbalance_2 == ($scope.balances_2 + blue + green + $scope.balancis_2_yellow)) {
          $scope.nextActiveButtonViaNotes2 = true;
        }else {
          $scope.nextActiveButtonViaNotes2 = false;
        }
        $scope.activeSubmitViaNotes(2);
      }
    }

    $scope.aditionalBalances_3 = function(id) {
      var val = ($('#'+id).val() == null || $('#'+id).val() == '') ? 0 : $('#'+id).val();
      var min = 0;
      if (id == 'balancis_3_green') {
        var red = ($scope.balancis_3_red == null || $scope.balancis_3_red == '') ? 0 : $scope.balancis_3_red;
        var yellow = ($scope.balancis_3_yellow == null || $scope.balancis_3_yellow == '') ? 0 : $scope.balancis_3_yellow;
        var max = $scope.cashbalance_3 - ($scope.balances_3 + red + yellow);
        if (max <= 0) {
          $('#'+id).val(0);
          $scope.balancis_3_green = 0;
          $('#background-balancis-green-3').css('background-color','#dedede');
        }else {
          if (val > max) {
            if (max > $scope.balances_1) {
              $('#'+id).val($scope.balances_1);
              $scope.balancis_3_green = $scope.balances_1;
            }else {
              $('#'+id).val(max);
              $scope.balancis_3_green = max;
            }
            $('#background-balancis-green-3').css('background-color','#71b58b');
          }else if (val < min) {
            $('#'+id).val(0);
            $scope.balancis_3_green = 0;
            $('#background-balancis-green-3').css('background-color','#dedede');
          }else {
            if (val == null || val == '' || val == 0) {
              $('#'+id).val(0);
              $scope.balancis_3_green = 0;
              $('#background-balancis-green-3').css('background-color','#dedede');
            }else {
              $('#background-balancis-green-3').css('background-color','#71b58b');
            }
          }
        }
        if ($scope.cashbalance_3 == ($scope.balances_3 + red + yellow + $scope.balancis_3_green)) {
          $scope.nextActiveButtonViaNotes3 = true;
        }else {
          $scope.nextActiveButtonViaNotes3 = false;
        }
        $scope.activeSubmitViaNotes(3);
      }else if (id == 'balancis_3_red') {
        var green = ($scope.balancis_3_green == null || $scope.balancis_3_green == '') ? 0 : $scope.balancis_3_green;
        var yellow = ($scope.balancis_3_yellow == null || $scope.balancis_3_yellow == '') ? 0 : $scope.balancis_3_yellow;
        var max = $scope.cashbalance_3 - ($scope.balances_3 + green + yellow);
        if (max <= 0) {
          $('#'+id).val(0);
          $scope.balancis_3_red = 0;
          $('#background-balancis-red-3').css('background-color','#dedede');
        }else {
          if (val > max) {
            if (max > $scope.balances_2) {
              $('#'+id).val($scope.balances_2);
              $scope.balancis_3_red = $scope.balances_2;
            }else {
              $('#'+id).val(max);
              $scope.balancis_3_red = max;
            }
            $('#background-balancis-red-3').css('background-color','#ff7388');
          }else if (val < min) {
            $('#'+id).val(0);
            $scope.balancis_3_red = 0;
            $('#background-balancis-red-3').css('background-color','#dedede');
          }else {
            if (val == null || val == '' || val == 0) {
              $('#'+id).val(0);
              $scope.balancis_3_red = 0;
              $('#background-balancis-red-3').css('background-color','#dedede');
            }else {
              $('#background-balancis-red-3').css('background-color','#ff7388');
            }
          }
        }
        if ($scope.cashbalance_3 == ($scope.balances_3 + green + yellow + $scope.balancis_3_red)) {
          $scope.nextActiveButtonViaNotes3 = true;
        }else {
          $scope.nextActiveButtonViaNotes3 = false;
        }
        $scope.activeSubmitViaNotes(3);
      }else if (id == 'balancis_3_yellow') {
        var green = ($scope.balancis_3_green == null || $scope.balancis_3_green == '') ? 0 : $scope.balancis_3_green;
        var red = ($scope.balancis_3_red == null || $scope.balancis_3_red == '') ? 0 : $scope.balancis_3_red;
        var max = $scope.cashbalance_3 - ($scope.balances_3 + green + red);
        if (max <= 0) {
          $('#'+id).val(0);
          $scope.balancis_3_yellow = 0;
          $('#background-balancis-yellow-3').css('background-color','#dedede');
        }else {
          if (val > max) {
            if (max > $scope.balances_4) {
              $('#'+id).val($scope.balances_4);
              $scope.balancis_3_yellow = $scope.balances_4;
            }else {
              $('#'+id).val(max);
              $scope.balancis_3_yellow = max;
            }
            $('#background-balancis-yellow-3').css('background-color','#f9db0a');
          }else if (val < min) {
            $('#'+id).val(0);
            $scope.balancis_3_yellow = 0;
            $('#background-balancis-yellow-3').css('background-color','#dedede');
          }else {
            if (val == null || val == '' || val == 0) {
              $('#'+id).val(0);
              $scope.balancis_3_yellow = 0;
              $('#background-balancis-yellow-3').css('background-color','#dedede');
            }else {
              $('#background-balancis-yellow-3').css('background-color','#f9db0a');
            }
          }
        }
        if ($scope.cashbalance_3 == ($scope.balances_3 + red + green + $scope.balancis_3_yellow)) {
          $scope.nextActiveButtonViaNotes3 = true;
        }else {
          $scope.nextActiveButtonViaNotes3 = false;
        }
        $scope.activeSubmitViaNotes(3);
      }
    }

    $scope.aditionalBalances_4 = function(id) {
      var val = ($('#'+id).val() == null || $('#'+id).val() == '') ? 0 : $('#'+id).val();
      var min = 0;
      if (id == 'balancis_4_green') {
        var red = ($scope.balancis_4_red == null || $scope.balancis_4_red == '') ? 0 : $scope.balancis_4_red;
        var blue = ($scope.balancis_4_blue == null || $scope.balancis_4_blue == '') ? 0 : $scope.balancis_4_blue;
        var max = $scope.cashbalance_4 - ($scope.balances_4 + red + blue);
        if (max <= 0) {
          $('#'+id).val(0);
          $scope.balancis_4_green = 0;
          $('#background-balancis-green-4').css('background-color','#dedede');
        }else {
          if (val > max) {
            if (max > $scope.balances_1) {
              $('#'+id).val($scope.balances_1);
              $scope.balancis_4_green = $scope.balances_1;
            }else {
              $('#'+id).val(max);
              $scope.balancis_4_green = max;
            }
            $('#background-balancis-green-4').css('background-color','#71b58b');
          }else if (val < min) {
            $('#'+id).val(0);
            $scope.balancis_4_green = 0;
            $('#background-balancis-green-4').css('background-color','#dedede');
          }else {
            if (val == null || val == '' || val == 0) {
              $('#'+id).val(0);
              $scope.balancis_4_green = 0;
              $('#background-balancis-green-4').css('background-color','#dedede');
            }else {
              $('#background-balancis-green-4').css('background-color','#71b58b');
            }
          }
        }
        if ($scope.cashbalance_4 == ($scope.balances_4 + red + blue + $scope.balancis_4_green)) {
          $scope.nextActiveButtonViaNotes4 = true;
        }else {
          $scope.nextActiveButtonViaNotes4 = false;
        }
        $scope.activeSubmitViaNotes(4);
      }else if (id == 'balancis_4_red') {
        var green = ($scope.balancis_4_green == null || $scope.balancis_4_green == '') ? 0 : $scope.balancis_4_green;
        var blue = ($scope.balancis_4_blue == null || $scope.balancis_4_blue == '') ? 0 : $scope.balancis_4_blue;
        var max = $scope.cashbalance_4 - ($scope.balances_4 + green + blue);
        if (max <= 0) {
          $('#'+id).val(0);
          $scope.balancis_4_red = 0;
          $('#background-balancis-red-4').css('background-color','#dedede');
        }else {
          if (val > max) {
            if (max > $scope.balances_2) {
              $('#'+id).val($scope.balances_2);
              $scope.balancis_4_red = $scope.balances_2;
            }else {
              $('#'+id).val(max);
              $scope.balancis_4_red = max;
            }
            $('#background-balancis-red-4').css('background-color','#ff7388');
          }else if (val < min) {
            $('#'+id).val(0);
            $scope.balancis_4_red = 0;
            $('#background-balancis-red-4').css('background-color','#dedede');
          }else {
            if (val == null || val == '' || val == 0) {
              $('#'+id).val(0);
              $scope.balancis_4_red = 0;
              $('#background-balancis-red-4').css('background-color','#dedede');
            }else {
              $('#background-balancis-red-4').css('background-color','#ff7388');
            }
          }
        }
        if ($scope.cashbalance_4 == ($scope.balances_4 + green + blue + $scope.balancis_4_red)) {
          $scope.nextActiveButtonViaNotes4 = true;
        }else {
          $scope.nextActiveButtonViaNotes4 = false;
        }
        $scope.activeSubmitViaNotes(4);
      }else if (id == 'balancis_4_blue') {
        var green = ($scope.balancis_4_green == null || $scope.balancis_4_green == '') ? 0 : $scope.balancis_4_green;
        var red = ($scope.balancis_4_red == null || $scope.balancis_4_red == '') ? 0 : $scope.balancis_4_red;
        var max = $scope.cashbalance_4 - ($scope.balances_4 + green + red);
        if (max <= 0) {
          $('#'+id).val(0);
          $scope.balancis_4_blue = 0;
          $('#background-balancis-blue-4').css('background-color','#dedede');
        }else {
          if (val > max) {
            if (max > $scope.balances_3) {
              $('#'+id).val($scope.balances_3);
              $scope.balancis_4_blue = $scope.balances_3;
            }else {
              $('#'+id).val(max);
              $scope.balancis_4_blue = max;
            }
            $('#background-balancis-blue-4').css('background-color','#00bcd4');
          }else if (val < min) {
            $('#'+id).val(0);
            $scope.balancis_4_blue = 0;
            $('#background-balancis-blue-4').css('background-color','#dedede');
          }else {
            if (val == null || val == '' || val == 0) {
              $('#'+id).val(0);
              $scope.balancis_4_blue = 0;
              $('#background-balancis-blue-4').css('background-color','#dedede');
            }else {
              $('#background-balancis-blue-4').css('background-color','#00bcd4');
            }
          }
        }
        if ($scope.cashbalance_4 == ($scope.balances_4 + red + green + $scope.balancis_4_blue)) {
          $scope.nextActiveButtonViaNotes4 = true;
        }else {
          $scope.nextActiveButtonViaNotes4 = false;
        }
        $scope.activeSubmitViaNotes(4);
      }
    }

    $scope.activeSubmitViaNotes = function(pocket_id) {
      if (pocket_id == 1) {
        if ($scope.notes_1 != null && $scope.notes_1 != '') {
          if ($scope.nextActiveButtonViaNotes1) {
            $scope.buttonSubmit_1 = true;
          }else {
            $scope.buttonSubmit_1 = false;
          }
        }else {
          $scope.buttonSubmit_1 = false;
        }
      }else if (pocket_id == 2) {
        if ($scope.notes_2 != null && $scope.notes_2 != '') {
          if ($scope.nextActiveButtonViaNotes2) {
            $scope.buttonSubmit_2 = true;
          }else {
            $scope.buttonSubmit_2 = false;
          }
        }else {
          $scope.buttonSubmit_2 = false;
        }
      }else if (pocket_id == 3) {
        if ($scope.notes_3 != null && $scope.notes_3 != '') {
          if ($scope.nextActiveButtonViaNotes3) {
            $scope.buttonSubmit_3 = true;
          }else {
            $scope.buttonSubmit_3 = false;
          }
        }else {
          $scope.buttonSubmit_3 = false;
        }
      }else if (pocket_id == 4) {
        if ($scope.notes_4 != null && $scope.notes_4 != '') {
          if ($scope.nextActiveButtonViaNotes4) {
            $scope.buttonSubmit_4 = true;
          }else {
            $scope.buttonSubmit_4 = false;
          }
        }else {
          $scope.buttonSubmit_4 = false;
        }
      }
    }

    //submit ballance 2,3,4
    $scope.submitBalances = function(pocket_id) {
      if (pocket_id == 1) {
        var transaction_type_id_1 = $scope.transaction_type_id_1;
        var notes_1 = $scope.notes_1;
        var purpose_id_1 = $scope.purpose_id_1;
        if (transaction_type_id_1 == 1) {
          var pockets_1 = [{
            pocket: 1,
            amount: $scope.cashbalance_1
          }]
        }else {
          var pockets_1 = [
            {pocket: 1,amount: $scope.cashbalance_1 > $scope.balances_1 ? $scope.balances_1 : $scope.cashbalance_1},
            {pocket: 2,amount: ($scope.balancis_1_red == null || $scope.balancis_1_red == '') ? 0 : $scope.balancis_1_red},
            {pocket: 3,amount: ($scope.balancis_1_blue == null || $scope.balancis_1_blue == '') ? 0 : $scope.balancis_1_blue},
            {pocket: 4,amount: ($scope.balancis_1_yellow == null || $scope.balancis_1_yellow == '') ? 0 : $scope.balancis_1_yellow},
        ]
        }
        $scope.closePocket1();
        $scope.loader = true;
        $.ajax({
                type: 'POST',
                url: CONF.transactions+$cookies.get('user_id')+'/transactions?transaction_type_id='+transaction_type_id_1,
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({
                  purpose_id: purpose_id_1,
                  pocket_id: pocket_id,
                  notes: notes_1,
                  pockets: JSON.stringify(pockets_1),
                }),
                headers: {
                  'User-Token': $cookies.get('token'),
                },
        })
        .done(function(data) {
          $scope.loader = false;
          if (!data.error) {
            $scope.$apply(function () {
             $scope.success_message = data.message;
            });
            success_transaction.showModal();
            success_transaction.addEventListener('click', function (event) {
                var rect = success_transaction.getBoundingClientRect();
                var isInDialog=(rect.top <= event.clientY && event.clientY <= rect.top + rect.height
                  && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
                if (!isInDialog && document.getElementById("success_transaction").open) {
                    $scope.getBalances();
                    success_transaction.close();
                }
            });
          }else {
            $scope.$apply(function () {
             $scope.error_message = data.message;
            });
            error_dialog.showModal();
          }
        })
        .fail(function(data) {
          $scope.loader = false;
          $scope.$apply(function () {
           $scope.error_message = JSON.parse(data.responseText)['message'];
          });
          error_dialog.showModal();
        })
      }else if (pocket_id == 2) {
        var transaction_type_id_2 = $scope.transaction_type_id_2;
        var notes_2 = $scope.notes_2;
        var purpose_id_2 = $scope.purpose_id_2;
        if (transaction_type_id_2 == 1) {
          var pockets_2 = [{
            pocket: 2,
            amount: $scope.cashbalance_2
          }]
        }else {
          var pockets_2 = [
            {pocket: 1,amount: ($scope.balancis_2_green == null || $scope.balancis_2_green == '') ? 0 : $scope.balancis_2_green},
            {pocket: 2,amount: $scope.cashbalance_2 > $scope.balances_2 ? $scope.balances_2 : $scope.cashbalance_2},
            {pocket: 3,amount: ($scope.balancis_2_blue == null || $scope.balancis_2_blue == '') ? 0 : $scope.balancis_2_blue},
            {pocket: 4,amount: ($scope.balancis_2_yellow == null || $scope.balancis_2_yellow == '') ? 0 : $scope.balancis_2_yellow},
        ]
        }
        $scope.closePocket2();
        $scope.loader = true;
        $.ajax({
                type: 'POST',
                url: CONF.transactions+$cookies.get('user_id')+'/transactions?transaction_type_id='+transaction_type_id_2,
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({
                  purpose_id: purpose_id_2,
                  pocket_id: pocket_id,
                  notes: notes_2,
                  pockets: JSON.stringify(pockets_2),
                }),
                headers: {
                  'User-Token': $cookies.get('token'),
                },
        })
        .done(function(data) {
          $scope.loader = false;
          if (!data.error) {
            $scope.$apply(function () {
             $scope.success_message = data.message;
            });
            success_transaction.showModal();
            success_transaction.addEventListener('click', function (event) {
                var rect = success_transaction.getBoundingClientRect();
                var isInDialog=(rect.top <= event.clientY && event.clientY <= rect.top + rect.height
                  && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
                if (!isInDialog && document.getElementById("success_transaction").open) {
                    $scope.getBalances();
                    success_transaction.close();
                }
            });
          }else {
            $scope.$apply(function () {
             $scope.error_message = data.message;
            });
            error_dialog.showModal();
          }
        })
        .fail(function(data) {
          $scope.loader = false;
          $scope.$apply(function () {
           $scope.error_message = JSON.parse(data.responseText)['message'];
          });
          error_dialog.showModal();
        })
      }else if (pocket_id == 3) {
        var transaction_type_id_3 = $scope.transaction_type_id_3;
        var notes_3 = $scope.notes_3;
        var purpose_id_3 = $scope.purpose_id_3;
        if (transaction_type_id_3 == 1) {
          var pockets_3 = [{
            pocket: 3,
            amount: $scope.cashbalance_3
          }]
        }else {
          var pockets_3 = [
            {pocket: 1,amount: ($scope.balancis_3_green == null || $scope.balancis_3_green == '') ? 0 : $scope.balancis_3_green},
            {pocket: 2,amount: ($scope.balancis_3_red == null || $scope.balancis_3_red == '') ? 0 : $scope.balancis_3_red},
            {pocket: 3,amount: $scope.cashbalance_3 > $scope.balances_3 ? $scope.balances_3 : $scope.cashbalance_3},
            {pocket: 4,amount: ($scope.balancis_3_yellow == null || $scope.balancis_3_yellow == '') ? 0 : $scope.balancis_3_yellow},
        ]
        }
        $scope.closePocket3();
        $scope.loader = true;
        $.ajax({
                type: 'POST',
                url: CONF.transactions+$cookies.get('user_id')+'/transactions?transaction_type_id='+transaction_type_id_3,
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({
                  purpose_id: purpose_id_3,
                  pocket_id: pocket_id,
                  notes: notes_3,
                  pockets: JSON.stringify(pockets_3),
                }),
                headers: {
                  'User-Token': $cookies.get('token'),
                },
        })
        .done(function(data) {
          $scope.loader = false;
          if (!data.error) {
            $scope.$apply(function () {
             $scope.success_message = data.message;
            });
            success_transaction.showModal();
            success_transaction.addEventListener('click', function (event) {
                var rect = success_transaction.getBoundingClientRect();
                var isInDialog=(rect.top <= event.clientY && event.clientY <= rect.top + rect.height
                  && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
                if (!isInDialog && document.getElementById("success_transaction").open) {
                    $scope.getBalances();
                    success_transaction.close();
                }
            });
          }else {
            $scope.$apply(function () {
             $scope.error_message = data.message;
            });
            error_dialog.showModal();
          }
        })
        .fail(function(data) {
          $scope.loader = false;
          $scope.$apply(function () {
           $scope.error_message = JSON.parse(data.responseText)['message'];
          });
          error_dialog.showModal();
        })
      }else if (pocket_id == 4) {
        var transaction_type_id_4 = $scope.transaction_type_id_4;
        var notes_4 = $scope.notes_4;
        var purpose_id_4 = $scope.purpose_id_4;
        if (transaction_type_id_4 == 1) {
          var pockets_4 = [{
            pocket: 4,
            amount: $scope.cashbalance_4
          }]
        }else {
          var pockets_4 = [
            {pocket: 1,amount: ($scope.balancis_4_green == null || $scope.balancis_4_green == '') ? 0 : $scope.balancis_4_green},
            {pocket: 2,amount: ($scope.balancis_4_red == null || $scope.balancis_4_red == '') ? 0 : $scope.balancis_4_red},
            {pocket: 3,amount: ($scope.balancis_4_blue == null || $scope.balancis_4_blue == '') ? 0 : $scope.balancis_4_blue},
            {pocket: 4,amount: $scope.cashbalance_4 > $scope.balances_4 ? $scope.balances_4 : $scope.cashbalance_4},
        ]
        }
        $scope.closePocket4();
        $scope.loader = true;
        $.ajax({
                type: 'POST',
                url: CONF.transactions+$cookies.get('user_id')+'/transactions?transaction_type_id='+transaction_type_id_4,
                contentType: 'application/json;charset=utf-8',
                dataType: 'json',
                data: JSON.stringify({
                  purpose_id: purpose_id_4,
                  pocket_id: pocket_id,
                  notes: notes_4,
                  pockets: JSON.stringify(pockets_4),
                }),
                headers: {
                  'User-Token': $cookies.get('token'),
                },
        })
        .done(function(data) {
          $scope.loader = false;
          if (!data.error) {
            $scope.$apply(function () {
             $scope.success_message = data.message;
            });
            success_transaction.showModal();
            success_transaction.addEventListener('click', function (event) {
                var rect = success_transaction.getBoundingClientRect();
                var isInDialog=(rect.top <= event.clientY && event.clientY <= rect.top + rect.height
                  && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
                if (!isInDialog && document.getElementById("success_transaction").open) {
                    $scope.getBalances();
                    success_transaction.close();
                }
            });
          }else {
            $scope.$apply(function () {
             $scope.error_message = data.message;
            });
            error_dialog.showModal();
          }
        })
        .fail(function(data) {
          $scope.loader = false;
          $scope.$apply(function () {
           $scope.error_message = JSON.parse(data.responseText)['message'];
          });
          error_dialog.showModal();
        })
      }
    }

    $scope.closeModal = function() {
      error_dialog.close();
    }
    $scope.closePocket1 = function() {
      pocket_1.close();
    }
    $scope.closePocket2 = function() {
      pocket_2.close();
    }
    $scope.closePocket3 = function() {
      pocket_3.close();
    }
    $scope.closePocket4 = function() {
      pocket_4.close();
    }

    $scope.goWallet = function() {
      $state.go('app.wallet',{obj: true});
    }

    $scope.goHistory = function() {
      $state.go('app.history',{obj: true});
    }

    $scope.goLogout = function() {
      $state.go('app.logout');
    }
  }
}]);

app.controller("goodCashHistory",['$scope', '$location', '$sessionStorage', '$localStorage', '$timeout', '$state', 'CONF','$http','goodCashAuth','$cookies', function($scope, $location, $sessionStorage, $localStorage, $timeout,$state,CONF,$http,goodCashAuth,$cookies){
  if($state.params.obj != null){
    if ($state.params.obj) {
      $scope.isauthenticatedHistory = true;
    }else {
      $state.go('app.login');
    }
  }else {
    if(goodCashAuth.isAuth()){
      $scope.isauthenticatedHistory = true;
    }else {
      $state.go('app.login');
    }
  }
  if ($scope.isauthenticatedHistory) {
    var sort_dialog = document.querySelector('#sort_dialog');
    var filter_dialog = document.querySelector('#filter_dialog');
    if (! sort_dialog.showModal) {
      dialogPolyfill.registerDialog(sort_dialog);
    }
    if (! filter_dialog.showModal) {
      dialogPolyfill.registerDialog(filter_dialog);
    }

    $scope.username = $cookies.get('username');
    $scope.loader = false;
    $scope.loadingHistory = false;
    $scope.order_by = 'date';
    $scope.sort_by = 'desc';
    $scope.filter = 'pockets';
    $scope.last_id = 0;
    $scope.page = 1;
    $scope.pockets_param = '1,2,3,4';
    $scope.purposes_param = '1,2,3,4';
    $scope.total_transactions = 0;

    function load(page) {
      // $scope.$apply(function () {
        $scope.loadingHistory = true;
      // });
      $.ajax({
              type: 'GET',
              url: CONF.transactions+$cookies.get('user_id')+'/transactions'+page,
              headers: {
                'User-Token': $cookies.get('token'),
              },
      })
      .done(function(data) {
        $scope.$apply(function () {
          $scope.loadingHistory = false;
        })
        if (!data.error) {
          $scope.page += 1;
          $scope.$apply(function () {
            $scope.total_transactions = data.total_transactions;
            $scope.items = $scope.items || [];
            $scope.items.push.apply($scope.items, data.data);
            $scope.last_id = 4*($scope.page - 1);
          });
        }
        $('#list_history').css('overflow', 'scroll');
      })
      .fail(function(data) {
        $scope.$apply(function () {
          $scope.loadingHistory = false;
        })
        $('#list_history').css('overflow', 'scroll');
      })
    }

    load('?offset='+$scope.last_id+'&order_by='+$scope.order_by+'&sort_by='+$scope.sort_by+'&pockets='+$scope.pockets_param+'&purposes='+$scope.purposes_param);

    angular.element(document.querySelector('#list_history')).on('scroll',function() {
      if ((document.querySelector('#list_history').scrollTop + document.querySelector('#list_history').offsetHeight) == document.querySelector('#list_history').scrollHeight) {
        $('#list_history').css('overflow', 'hidden');
        load('?offset='+$scope.last_id+'&order_by='+$scope.order_by+'&sort_by='+$scope.sort_by+'&pockets='+$scope.pockets_param+'&purposes='+$scope.purposes_param);
      }
    });

    $scope.showSortDialog = function() {
      sort_dialog.showModal();
      sort_dialog.addEventListener('click', function (event) {
          var rect = sort_dialog.getBoundingClientRect();
          var isInDialog=(rect.top <= event.clientY && event.clientY <= rect.top + rect.height
            && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
          if (!isInDialog && document.getElementById("sort_dialog").open) {
              sort_dialog.close();
          }
      });
    }

    $scope.showFilterDialog = function() {
      filter_dialog.showModal();
      filter_dialog.addEventListener('click', function (event) {
          var rect = filter_dialog.getBoundingClientRect();
          var isInDialog=(rect.top <= event.clientY && event.clientY <= rect.top + rect.height
            && rect.left <= event.clientX && event.clientX <= rect.left + rect.width);
          if (!isInDialog && document.getElementById("filter_dialog").open) {
              filter_dialog.close();
          }
      });
    }

    $scope.applySort = function() {
      if ($scope.order_by == 'date') {
        $scope.sort_by = $scope.sort.date;
      }else {
        $scope.sort_by = $scope.sort.amount;
      }
      sort_dialog.close();

      $scope.items = [];
      $scope.last_id = 0;
      $scope.page = 1;
      load('?offset='+$scope.last_id+'&order_by='+$scope.order_by+'&sort_by='+$scope.sort_by+'&pockets='+$scope.pockets_param+'&purposes='+$scope.purposes_param);
    }

    $scope.applyFilter = function() {
      if ($scope.filter == 'pockets') {
        $scope.pocket = [];
        if ($scope.pockets == null) {
          $scope.pocket = ["1","2","3","4"];
        }else {
          if ($scope.pockets.green) {
            $scope.pocket.push("1");
          }
          if ($scope.pockets.red) {
            $scope.pocket.push("2")
          }
          if ($scope.pockets.blue) {
            $scope.pocket.push("3")
          }
          if ($scope.pockets.yellow) {
            $scope.pocket.push("4")
          }
        }
        $scope.pockets_param = $scope.pocket.toString();
      }else {
        $scope.purpose = [];
        if ($scope.purposes == null) {
          $scope.purpose = ["1","2","3","4"];
        }else {
          if ($scope.purposes.food) {
            $scope.purpose.push("1")
          }
          if ($scope.purposes.transport) {
            $scope.purpose.push("2")
          }
          if ($scope.purposes.entertainment) {
            $scope.purpose.push("3")
          }
          if ($scope.purposes.other) {
            $scope.purpose.push("4")
          }
        }
        $scope.purposes_param = $scope.purpose.toString();
      }
      $scope.items = [];
      $scope.last_id = 0;
      $scope.page = 1;
      load('?offset='+$scope.last_id+'&order_by='+$scope.order_by+'&sort_by='+$scope.sort_by+'&pockets='+$scope.pockets_param+'&purposes='+$scope.purposes_param);
      filter_dialog.close();
    }

    $scope.sortDate = function() {
      $scope.order_by = 'date';
    }

    $scope.sortAmount = function() {
      $scope.order_by = 'amount';
    }

    $scope.filterPockets = function() {
      $scope.filter = 'pockets';
    }

    $scope.filterPurposes = function() {
      $scope.filter = 'purposes';
    }

    $scope.goWallet = function() {
      $state.go('app.wallet',{obj: true});
    }
    $scope.goHistory = function() {
      $state.go('app.history',{obj: true});
    }
    $scope.goLogout = function() {
      $state.go('app.logout');
    }
  }
}]);
