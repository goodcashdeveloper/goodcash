'use strict';

var configuration = angular.module('goodCash');

configuration.constant('CONF',
  {
    //API goodCash
    'login'             : 'http://goodcashapi.com/goodcashbackend/api/auth/tokens',
    'logout'            : 'http://goodcashapi.com/goodcashbackend/api/auth/tokens',
    'getAllUser'        : 'http://goodcashapi.com/goodcashbackend/api/users',
    'getUser'           : 'http://goodcashapi.com/goodcashbackend/api/users/',
    'createUser'        : 'http://goodcashapi.com/goodcashbackend/api/users',
    'editUser'          : 'http://goodcashapi.com/goodcashbackend/api/users/',
    'getbalances'       : 'http://goodcashapi.com/goodcashbackend/api/users/',
    'transactions'      : 'http://goodcashapi.com/goodcashbackend/api/users/',
    'api'               : 'xxx',
    'forgetPassword'    : 'http://goodcashapi.com/goodcashbackend/api/auth/passwords',
  }
);
